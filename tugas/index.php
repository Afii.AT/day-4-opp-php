<?php
    require_once 'animal.php';
    require_once 'frog&ape.php';
    $sheep = new animal("shaun", 2, 'false');

    echo $sheep->get_name(). '<br>'; // "shaun"
    echo $sheep->get_legs(). '<br>'; // 2
    echo $sheep->get_cold_blooded(). '<br>'; // false
    echo '<br><br>';

    $sungokong = new Ape("Kera sakti", 2, 'false');
    echo $sungokong-> get_name().'<br>';
    echo $sungokong-> get_legs().'<br>';
    echo $sungokong-> get_cold_blooded().'<br>';
    echo $sungokong-> get_yell().'<br><br>';

    $kodok = new Frog("buduk", 4, 'true');
    echo $kodok-> get_name().'<br>';
    echo $kodok-> get_legs().'<br>';
    echo $kodok-> get_cold_blooded().'<br>';
    echo $kodok-> get_jump();
?>
